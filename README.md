# SensesController README


O SensesController � uma aplica��o de gest�o, controlo e temporiza��o de atuadores ou digital outputs para Phidgets. 
Principal utiliza��o como controlador de sensa��es numa sala de cinema 4D.

A rotina a ser executada � definida num ficheiro em formato JSON ("routine.json") contendo uma lista de instru��es.

## Exemplo de instru��o:

{ "id":"3", "message":"Aroma", "time":5.0, "self":true, "outputID":2, "delay":5}

## Legenda:

	* id: Identificador �nico 

	* message: Descri��o 

	* time: tempo de in�cio da ativa��o da sensa��o (relativo ao arranque da aplica��o)

	* self: true= instru��o interna (sensa��o) ; false= instru��o externa (envia "message" por socket)

	* outputID: output a ser ativado (ex: identificador do rel� de um InterfaceKit)

	* delay: dura��o da ativa��o
