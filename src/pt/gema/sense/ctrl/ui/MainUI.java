package pt.gema.sense.ctrl.ui;

import java.awt.BorderLayout;
import java.awt.Color;

/**
 * @author Vin�cius Ginja
 */

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import pt.gema.sense.ctrl.SenseRoutine;
import pt.gema.sense.ctrl.phidgets.Phidgets;
import pt.gema.sense.ctrl.utils.Settings;
import pt.gema.sense.ctrl.utils.Shared;

import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;

public class MainUI {
	public JFrame win = null;
	
	public MainUI(){
		winDraw();
		
		win.setIconImage(Shared.icon128.getImage());
	}
	
	private void winDraw(){
		JButton startButton;
		JTextArea textArea;
		JScrollPane areaScrollPane;
		MessageConsole mc;
		
		win = new JFrame();
		
		setApplicationLookAndFeel();
		
	
		win.setTitle("Senses Controller");
		win.setSize(600, 300);
		win.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		win.setIconImage(Shared.icon128.getImage());
		
		startButton = new JButton("Run Sensation Routine");
		win.add(startButton,BorderLayout.SOUTH);
		
		textArea = new JTextArea(300,400);
		textArea.setEditable(false);
		
		areaScrollPane = new JScrollPane(textArea,
	            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		win.add( areaScrollPane);
		mc = new MessageConsole(textArea);
		mc.redirectOut();
		mc.redirectErr(Color.RED, null);
		mc.setMessageLines(100);
		
		win.add(textArea,BorderLayout.NORTH);
		
		startButton.addActionListener(new ActionListener(){  
			public void actionPerformed(ActionEvent e){  
				new SenseRoutine(Settings.getConfig(), Phidgets.getIk());
			}  
	    });  
		
		win.setVisible(true);
		
		centerWindow(win);
	}
	
	private void setApplicationLookAndFeel() {
		try {
			UIManager.put("Synthetica.license.info", new String[] { "Licensee=AppWork UG", "LicenseRegistrationNumber=289416475", "Product=Synthetica", "LicenseType=Small Business License", "ExpireDate=--.--.----", "MaxVersion=2.999.999" });
			UIManager.put("Synthetica.license.key", new String(new byte[] { 67, 49, 52, 49, 48, 50, 57, 52, 45, 54, 49, 66, 54, 52, 65, 65, 67, 45, 52, 66, 55, 68, 51, 48, 51, 57, 45, 56, 51, 52, 65, 56, 50, 65, 49, 45, 51, 55, 69, 53, 68, 54, 57, 53 }, "UTF-8"));
			UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
		} catch (Exception e) {
			e.printStackTrace();
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Exception ee) {
				System.out.println("Error setting native Look and Feel: " + ee);
			}
		}
	}
	
	public static void centerWindow(Window win) {
		Dimension dim = win.getToolkit().getScreenSize();
		win.setLocation(dim.width / 2 - win.getWidth() / 2, dim.height / 2 - win.getHeight() / 2);
	}
}