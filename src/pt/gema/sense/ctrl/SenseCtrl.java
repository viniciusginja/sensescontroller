package pt.gema.sense.ctrl;

import pt.gema.sense.ctrl.phidgets.Phidgets;
import pt.gema.sense.ctrl.ui.MainUI;
import pt.gema.sense.ctrl.utils.Settings;
import pt.gema.sense.ctrl.utils.Shared;

/**
 * @author Vin�cius Ginja
 */
public class SenseCtrl {
	
	public SenseCtrl() {
		//Shared.SenseCtrl = this;
		
		new Phidgets();
		
		//new SenseRoutine(Settings.getConfig(), Phidgets.getIk()); // is now trigerred by a button
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		
		new Settings();
		
		new Shared();
		
		new MainUI();
		
		new SenseCtrl();
		
	}
}
