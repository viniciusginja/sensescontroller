package pt.gema.sense.ctrl.vo;

/**
 * @author Vin�cius Ginja
 */

public class Sense {
	
	public int     id = 0;       // instruction ID
	public String  message = ""; // descriptor
	public double  time = 0;     // start time relative to program start
	public boolean self = true;  // true = sensation; false = send 'message' trough socket to GemaCore
	public int     outputID = 0; // output ID (digital)
	public int     delay = 0;    // duration of sensation
    
    public Sense(int id, String message, double time, boolean self, int outputID, int delay) {
    	this.id = id;
    	this.message = message;
    	this.time = time;
    	this.self = self;
    	this.outputID = outputID;
    	this.delay = delay;
    }
}