package pt.gema.sense.ctrl;

import java.net.*;
import java.io.*;

import pt.gema.sense.ctrl.SocketClient;
import pt.gema.sense.ctrl.interfaces.ISocketClient;
//import pt.gema.sense.ctrl.utils.ErrorCode;

/**
 * @author Toni Almeida
 */

public class SocketClient extends Thread {

	private static SocketClient socketClient = null;
	private Socket socket = null;
	private ISocketClient parent;
	private BufferedInputStream in;
	private boolean desonnected = false;

	public synchronized void setDesonnected(boolean cr) {
		desonnected = cr;
	}

	private SocketClient(Socket s, ISocketClient parent) {
		super("SocketClient");
		socket = s;
		this.parent = parent;
		setDesonnected(false);
		start();
	}

	public static synchronized SocketClient handle(Socket s, ISocketClient parent) {
		if (socketClient == null)
			socketClient = new SocketClient(s, parent);
		else {
			if (socketClient.socket != null) {
				try {
					socketClient.socket.close();
				} catch (Exception e) {
					//SocketClientUI.error(e.getMessage(), ErrorCode.CLIENT_CLOSING_SOCKET);
				}
			}
			socketClient.socket = null;
			socketClient = new SocketClient(s, parent);
		}
		return socketClient;
	}

	public void run() {
		InputStream is = null;
		try {
			is = socket.getInputStream();
			in = new BufferedInputStream(is);
		} catch (IOException e) {
			try {
				socket.close();
			} catch (IOException e2) {
				//SocketClientUI.error("Socket not closed: " + e2.getMessage(), ErrorCode.CLIENT_SOCKET_NOT_CLOSED);
			}
			//SocketClientUI.error("Could not open socket : " + e.getMessage(), ErrorCode.CLIENT_OPENING_SOCKET);
			parent.disconnect();
			return;
		}

		while (!desonnected) {
			try {
				String got = readInputStream(in); // in.readLine();
				if (got == null) {
					//SocketClientUI.error("Connection closed by client", ErrorCode.CLIENT_NO_DATA_CLOSED);
					parent.disconnect();
					break;
				}
				parent.messageReceived(got);
			} catch (IOException e) {
				if (!desonnected) {
					//SocketClientUI.error(e.getMessage(), ErrorCode.CLIENT_DATA_ERROR, "Connection lost");
					parent.disconnect();
				}
				break;
			}
		}
		try {
			is.close();
			in.close();
		} catch (Exception e) {
			//SocketClientUI.error("Socket not closed: " + e.getMessage(), ErrorCode.CLIENT_SOCKET_NOT_ENDED);
		}
		socket = null;
	}

	private static String readInputStream(BufferedInputStream _in) throws IOException {
		String data = "";
		int s = _in.read();
		if (s == -1) return null;
		data += "" + (char) s;
		int len = _in.available();
		if (len > 0) {
			byte[] byteData = new byte[len];
			_in.read(byteData);
			data += new String(byteData);
		}
		return data;
	}

}
