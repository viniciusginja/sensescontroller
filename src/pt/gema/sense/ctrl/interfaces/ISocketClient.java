package pt.gema.sense.ctrl.interfaces;

import java.awt.Component;

public interface ISocketClient {
	
	public Component win();

	public void disconnect();
	
	public void messageReceived(String msg);

	public boolean isConnected();

	public boolean isAutostart();

	public void sendMessage(String string);

	public void clearMsgIfNeeded();

	public void connect(String ip, int port);

	public void connect();
	
}