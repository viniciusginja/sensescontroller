package pt.gema.sense.ctrl.console;

import java.awt.Component;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

import pt.gema.sense.ctrl.SocketClient;
import pt.gema.sense.ctrl.utils.ErrorCode;
import pt.gema.sense.ctrl.utils.Text;
import pt.gema.sense.ctrl.interfaces.ISocketClient;

/**
 * @author Toni Almeida
 */

public class SocketClientConsole implements ISocketClient {
	public boolean isSecure = false;
	public boolean isAutoStart = false;
	public boolean isAutoSend = true;

	public Socket socket;
	private PrintWriter out;
	private SocketClient socketClient;

	public SocketClientConsole(String ip, int port) {
		 connect(ip, port);
	}

	public void connect(String ip, int port) {
		if (socket != null) {
			disconnect();
			return;
		}
		
		try {
			socket = new Socket(ip, port);
			
			try {
				NetworkInterface network = NetworkInterface.getByInetAddress(InetAddress.getLocalHost());
				byte[] mac = network.getHardwareAddress();
				
				StringBuilder smac = new StringBuilder();
				for (int i = 0; i < mac.length; i++)
					smac.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "." : ""));
				
				sendMessage("CLI#" + smac + "#" + InetAddress.getLocalHost().getHostName() + "#");
			} catch (Exception e) {
				error("Unable to send MAC address", ErrorCode.CLIENT_MAC_ADDRESS);
			}
			
		} catch (Exception e) {
			error(e.getMessage(), ErrorCode.CLIENT_OPENING_CONNECTION, "Opening connection");
			return;
		}
		socketClient = SocketClient.handle(socket, this);
	}

	public synchronized void disconnect() {
		if(socket != null) {
			try {
				socketClient.setDesonnected(true);
				socket.close();
			} catch (Exception e) {
				error("Error closing client", ErrorCode.CLIENT_CLOSING_CONNECTION);
				System.err.println("Error closing client : " + e);
			}
			socket = null;
			out = null;
		}
	}

	public static void error(String error, int errorCode) {
		error(error, errorCode, "");
	}

	public static void error(String error, int errorCode, String heading) {
		if (error == null || error.equals("")) return;
		append("[ERR] " + String.valueOf(errorCode) + ": " + error);
	}

	public static void append(String msg) {
		append(msg, true);
	}
	
	public static void append(String msg, boolean newLine) {
		if(msg.length() > 1) {
			String r = (new SimpleDateFormat("HH:mm.ss").format(new Date())) + " " + msg.replace("|", "") + (newLine ? Text.newLine : "");
			System.out.print(r);
		}
	}
	
	public void messageReceived(String msg) {
		msg = Text.clean(msg);
		if(msg.length() > 0)
			append(msg);
	}

	public void sendMessage(String s) {
		try {
			if(out == null) out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
			s = Text.clean(s);
			if(s.length() > 0)
				append("> " + s);
			
			out.print(s + "|");
			out.flush();
		} catch (Exception e) {
	    	error("Unable to send message!", ErrorCode.CLIENT_NO_MESSAGE);
			disconnect();
		}
	}
	
	public boolean isConnected(){
		return socket != null && socket.isConnected();
	}
	

	public void clearMsgIfNeeded() {
		
	}
	
	public Component win() {
		return null;
	}
	
	public boolean isAutostart() {
		return isAutoStart;
	}
	
	public void connect() {
		throw new Error();
	}
}
