package pt.gema.sense.ctrl;

import java.util.Timer; 
import java.util.TimerTask;

import com.phidgets.InterfaceKitPhidget;
import com.phidgets.PhidgetException;

import pt.gema.sense.ctrl.vo.Sense;

/**
 * @author Vin�cius Ginja
 */

public class SenseTimer extends TimerTask {
	private Timer timer;
	private long timeStartSense;
	private int outputID;
	private Boolean kill;
	private InterfaceKitPhidget ikTarget;
	
	// TO DO: remove startTime arg if not used, and timeStartSense var
	public SenseTimer(long startTime, Sense instruction, InterfaceKitPhidget ik) {
		kill = false;
		// get output to be timed
		outputID = instruction.outputID;
		// get target InterfaceKit
		ikTarget = ik;
		// initialize, configure and start timer
		timer = new Timer();
		timeStartSense = (long)(instruction.time*1000) ;
		timer.schedule(this, timeStartSense, (long)(instruction.delay*1000));
	}

	public void run()
	{
		try {
			// second (last) tick, deassert output
			if (kill) {
				ikTarget.setOutputState(outputID, false);
				timer.cancel();
				kill = false;
			}
			// first tick, assert output
			else {
				ikTarget.setOutputState(outputID, true);
				kill = true;
			}
		}
		catch (PhidgetException e) {
			e.printStackTrace();
		}
	}
}
