package pt.gema.sense.ctrl.phidgets;

import com.phidgets.*;

/**
 * @author Vin�cius Ginja
 */

public class Phidgets {
	//--------------------------------------------------------------------------
	//
	//  Variables
	//
	//--------------------------------------------------------------------------
    private static InterfaceKitPhidget ik;
    public static IFKitDetachListener detach_listener;
    public static IFKitErrorListener error_listener;
    
	/**
	 *  Constructor
	 */
	public Phidgets( ) 
	{
		// initialize listeners
		error_listener   = new IFKitErrorListener();
		detach_listener  = new IFKitDetachListener();
		
		// setup phidget (InterfaceKit)
		start();
	}
	
	//--------------------------------------------------------------------------
    //
	//  Methods
	//
	//--------------------------------------------------------------------------
	public void start() 
	{
		try {
			
			ik = new InterfaceKitPhidget();
			
			System.out.println("Connecting to Phidgets...");
			ik.openAny(); 
			//ik.open(385430); 
			//ik.open(385430,"test");
			//ik.openAny("test");
			
			System.out.println("Waiting InterfaceKit attachment...");
			ik.waitForAttachment();
			System.out.println(ik.getDeviceName() + ": Attached!");
			
			// make sure outputs are off (target scenario: app is re-runed after being closed during routine w/ phidget still attached)
			setOutputStateAll(false);
			
			// add listeners
			ik.addDetachListener(detach_listener); // also handles AttatchListener (mutually exclusive) 
			ik.addErrorListener(error_listener);
		
	    } catch (PhidgetException ex) {
	    	System.out.println(ex.getDescription());
        }
	}
 
	public static InterfaceKitPhidget getIk() {
		return ik;
	}
	
	public void setOutputStateAll(Boolean value) throws PhidgetException {
		for(int i = 0; i < ik.getOutputCount(); i++) 
			ik.setOutputState(i, value);
	}
}
