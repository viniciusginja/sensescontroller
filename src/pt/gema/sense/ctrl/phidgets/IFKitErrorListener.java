package pt.gema.sense.ctrl.phidgets;

import com.phidgets.event.ErrorListener;
import com.phidgets.event.ErrorEvent;

/**
 * @author Vin�cius Ginja
 */

public class IFKitErrorListener implements ErrorListener {
  
	public void error(ErrorEvent ee)
	{
		System.out.println(ee);
	} 
}