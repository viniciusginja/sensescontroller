package pt.gema.sense.ctrl.phidgets;

import com.phidgets.InterfaceKitPhidget;
import com.phidgets.PhidgetException;
import com.phidgets.event.AttachListener;
import com.phidgets.event.AttachEvent;

/**
 * @author Vin�cius Ginja
 */

public class IFKitAttachListener implements AttachListener {
	public static IFKitDetachListener dl = new IFKitDetachListener();

	public void attached(AttachEvent ae)
	{
		InterfaceKitPhidget ikSrc = (InterfaceKitPhidget) ae.getSource();

		System.out.println("attachment of " + ae);
		
		// make sure every output is off
		try {
			for(int i = 0; i < ikSrc.getOutputCount(); i++) 
				ikSrc.setOutputState(i, false);
		} catch (PhidgetException e) {
			e.printStackTrace();
		}
		
		ikSrc.removeAttachListener(this);
		ikSrc.addDetachListener(dl);
	} 
}