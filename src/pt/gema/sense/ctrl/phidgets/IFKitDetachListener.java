package pt.gema.sense.ctrl.phidgets;

import com.phidgets.InterfaceKitPhidget;
import com.phidgets.event.DetachListener;
import com.phidgets.event.DetachEvent;

/**
 * @author Vin�cius Ginja
 */

public class IFKitDetachListener implements DetachListener {
	public static IFKitAttachListener al = new IFKitAttachListener();

	public void detached(DetachEvent ae)
	{
		InterfaceKitPhidget ikSrc = (InterfaceKitPhidget) ae.getSource();
		
		System.out.println("detachment of " + ae);

		ikSrc.removeDetachListener(this);
		ikSrc.addAttachListener(al);
	} 
}