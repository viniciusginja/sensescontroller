package pt.gema.sense.ctrl.phidgets;

import com.phidgets.event.ServerConnectEvent;
import com.phidgets.event.ServerConnectListener;

/**
 * @author Vin�cius Ginja
 */

public class IFKitConnectListener implements ServerConnectListener {
  
  public void serverConnected(ServerConnectEvent ae) 
  {
	  System.out.println("connection of " + ae);
  } 
}