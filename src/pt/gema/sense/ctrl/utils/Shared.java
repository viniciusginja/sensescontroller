package pt.gema.sense.ctrl.utils;

import javax.swing.ImageIcon;

import pt.gema.sense.ctrl.SenseCtrl;
import pt.gema.sense.ctrl.utils.Shared;

/**
 * @author Vin�cius Ginja
 */

public class Shared
{
	public static SenseCtrl SenseCtrl;
	public static final String ASSETS = "assets/";
	public static ImageIcon icon128;
	
	public Shared()
	{
		// Images
		ClassLoader cl = getClass().getClassLoader();
		
		icon128 = new ImageIcon(cl.getResource(ASSETS + "icon_128.png"));
		
	}
	
}