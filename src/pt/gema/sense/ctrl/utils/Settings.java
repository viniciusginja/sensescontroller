package pt.gema.sense.ctrl.utils;

import java.io.FileReader;
import java.io.BufferedReader;

import org.json.*;

//import pt.gema.sense.ctrl.utils.ErrorCode;

/**
 * @author Vin�cius Ginja
 */

public class Settings  {	
	public static JSONArray routine;
	
	public Settings() {
		System.out.println("Loading Settings (Sensorial routine)");
		init();
	}

	private void init() {
		try {
			FileReader file = new FileReader("routine.json");
			BufferedReader buff = new BufferedReader(file); 
			// Create string from file
			String s, res = ""; 
			while((s = buff.readLine()) != null) res += s + '\n'; 
			file.close();
			// Create array of JSONObjects
			routine = new JSONArray(res);
		} catch (Exception e) {
			System.out.println("ERROR");
			//Shared.UIError("Settings file missing, setting up default values", ErrorCode.LOADING_SETTINGS, true);
		}
	}

	public static JSONArray getConfig() {
		return routine;
	}
}