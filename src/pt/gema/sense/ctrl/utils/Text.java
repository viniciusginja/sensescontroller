package pt.gema.sense.ctrl.utils;


/**
 * @author Toni Almeida
 */

public class Text {

	public static final String newLine = "\r\n";
	
	public Text() {
		
	}
	
	public static String clean(String msg) {
		return msg.replaceAll("\\r|\\n|\\||\\\\r|\\\\n", "");
	}
	
}
