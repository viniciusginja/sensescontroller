package pt.gema.sense.ctrl.utils;

/**
 * @author Toni Almeida
 */

public class ErrorCode {

	// ---------------------------------------------------------------------
	// 
	//		A						|	B				|	C
	//  	1		2		3		|	1		2		|	
	// 		Common	Client	Server	|	UI		Thread	|	Error Code
	// 
	// ---------------------------------------------------------------------
	

	public static final int SAVE_FILE 					= 101;
	public static final int UPDATE	 					= 102;
	public static final int DOWN_UPDATE	 				= 103;
	public static final int APPLY_UPDATE	 			= 104;
	public static final int SEND_MAIL	 				= 105;
	public static final int API_CALL	 				= 106;
	public static final int PREPARING_UPDATE			= 107;
	public static final int CERTIFICATE 				= 108;
	public static final int SETTINGS_NOT_SAVED 			= 109;
	public static final int CLOSING_SETTINGS 			= 110;
	public static final int LOADING_SETTINGS 			= 111;
	public static final int UNABLE_TO_GET_MAC 			= 112;
	public static final int UNABLE_TO_GET_HOST 			= 113;
	public static final int UNABLE_TO_GET_VERSION 		= 114;
	public static final int UNABLE_TO_SEND_MESSAGE 		= 115;
	public static final int DEFAULT_SETTINGS_NOT_SAVED  = 116;
	public static final int DEFAULT_NOT_FOUND           = 117;
	public static final int BACKUP_NOT_FOUND            = 118;
	
	// Client UI
	public static final int CLIENT_MAC_ADDRESS 			= 211;
	public static final int CLIENT_OPENING_CONNECTION 	= 212;
	public static final int CLIENT_CLOSING_CONNECTION 	= 213;
	public static final int CLIENT_OPENING_SOCKET 		= 214;
	public static final int CLIENT_NO_SHUTDOWN	 		= 215;
	public static final int CLIENT_NO_MESSAGE	 		= 216;
	
	// Client Thread
	public static final int CLIENT_CLOSING_SOCKET	 	= 221;
	public static final int CLIENT_NO_DATA_CLOSED	 	= 222;
	public static final int CLIENT_DATA_ERROR	 		= 223;
	public static final int CLIENT_SOCKET_NOT_CLOSED	= 224;
	public static final int CLIENT_SOCKET_NOT_ENDED		= 225;
	
	// Server UI
	public static final int SERVER_OPENING_CONNECTION 	= 311;
	public static final int SERVER_NO_CMD 				= 312;
	public static final int SERVER_NO_SERVICES 			= 313;
	public static final int SERVER_SHUTDOWN 			= 314;
	
	// Server Thread
	public static final int SERVER_CLOSING_SOCKET	 	= 321;
	public static final int SERVER_SOCKET_INIT			= 322;
	public static final int SERVER_SOCKET_LISTEN		= 323;
	public static final int SERVER_SOCKET_DISTROY		= 324;
	
	// Server Thread Optoma
	public static final int SERVER_SOCKET_PROJ_INIT		= 331;
	public static final int SERVER_SOCKET_PROJ_LISTEN	= 332;
	public static final int SERVER_SOCKET_PROJ_DISTROY	= 333;
	
	// Server WOL
	public static final int SERVER_WOL_PROJECTOR	 	= 341;
	public static final int SERVER_WOL_PACKET			= 342;
	public static final int SERVER_WOL_SHUTDOWN_PROJ	= 343;

	// ---------------------------------------------------------------------
	
	// Messages
	public static final String SERVER_OPENING_CONNECTION_MSG = "Your IP/Port must be in use or you may misspelled your IP address!";
	
}
