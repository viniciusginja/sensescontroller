package pt.gema.sense.ctrl;

import pt.gema.sense.ctrl.console.SocketClientConsole;
import pt.gema.sense.ctrl.interfaces.ISocketClient;
import pt.gema.sense.ctrl.vo.Sense;

import java.util.ArrayList;

import com.phidgets.InterfaceKitPhidget;

import org.json.*;

/**
 * @author Vin�cius Ginja
 */

public class SenseRoutine {
	public ISocketClient iSocketClient;
	private ArrayList<Sense> instructionsSet = new ArrayList<Sense>();
	private ArrayList<SenseTimer> instructionsTimers = new ArrayList<SenseTimer>();
	private Sense instructionToPerform;
	private int instructionCount;
	private long startTime;

	public SenseRoutine(JSONArray routine, InterfaceKitPhidget ik)
	{
		// connect to socket server (e.g. GemaCore)
		iSocketClient = new SocketClientConsole("127.0.0.1", 1300); // TO DO: remover prego (n�meros m�gicos)
		// get current time
		startTime = System.currentTimeMillis();
		// build instruction collection
		getInstructions(routine);
	    // perform instructions 
	    for(int i = 0; i < instructionCount; i++) {
	    	// get current instruction
	    	instructionToPerform = instructionsSet.get(i);
	    	// check if is a sensation
	    	if (instructionToPerform.self == true) {
	    		// create sensation timer
	    		SenseTimer timer = new SenseTimer(startTime, instructionToPerform, ik);
	    		// store in array list
	    		instructionsTimers.add(timer);
	    	}
	    	else {
	    		// send 'message' through socket
	    		iSocketClient.sendMessage(instructionToPerform.message);
	    	}
	    }
	}

	public void getInstructions(JSONArray routine)
	{
		// number of instructions
		instructionCount = routine.length();
		for(int i = 0; i < instructionCount; i++) {
			// get next instruction object
			JSONObject tmpObj = routine.getJSONObject(i);
			// build Sense entry
			Sense tmpSense = new Sense(tmpObj.getInt("id"),
									   tmpObj.getString("message"),
									   tmpObj.getDouble("time"),
									   tmpObj.getBoolean("self"),
									   tmpObj.getInt("outputID"),
									   tmpObj.getInt("delay")
									   );
			// add array entry
			instructionsSet.add(tmpSense);
		}
	}

}